package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;

import eeg.Main;
import eeg.MouseController;

public class SwingGUI extends JFrame implements Runnable {

	private static final long serialVersionUID = 4095956861320483448L;
	private Thread t;
	private final String THREAD_NAME = "GUIThread";

	private final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	private final int SCREEN_WIDTH = SCREEN_SIZE.width;
	private final int SCREEN_HEIGHT = SCREEN_SIZE.height;
	private final int SCREEN_WIDTH_MIDDLE = SCREEN_WIDTH / 2;
	private final int SCREEN_HEIGHT_MIDDLE = SCREEN_HEIGHT / 2;

	private final int TASKBAR_HEIGHT = 40;

	private final double BUTTON_SHRINK_PERCENTAGE = 0.25;
	private final int BUTTON_STARTING_WIDTH = 100;
	private final int BUTTON_STARTING_HEIGHT = 80;

	private final int INITAL_DELAY = 2000; // 2000
	private final int MAXIMUM_DELAY = 3000; // 3000

	private final int TIMER_ARRAY_SIZE = 10;

	private JPanel panel;
	private JButton precisionTestButton;
	private JButton reactionTestButton;
	private JButton precisionTestStartButton;
	private JButton reactionTestStartButton;
	private int buttonWidth;
	private int buttonHeight;

	private PrecisionSetupButtonListener precisionSetupButtonListener = new PrecisionSetupButtonListener();
	private PrecisionStartButtonListener precisionStartButtonListener = new PrecisionStartButtonListener();
	private PrecisionButtonListener precisionButtonListener = new PrecisionButtonListener();

	private ReactionSetupButtonListener reactionSetupButtonListener = new ReactionSetupButtonListener();
	private ReactionButtonListener reactionButtonListener = new ReactionButtonListener();

	private double[] timerArray;
	private int currentField;
	private int precisionTestIteration = 0;
	private int reactionTestIteration = 0;

	private long startTime;
	private long endTime;

	private String currentEmote = "brow";

	public SwingGUI() {
		super("eeg headset testing environment");
	}

	@Override
	public void run() {

		currentField = 0;
		timerArray = new double[TIMER_ARRAY_SIZE];

		buttonWidth = BUTTON_STARTING_WIDTH;
		buttonHeight = BUTTON_STARTING_HEIGHT;

		setSize(SCREEN_SIZE);
		setExtendedState(MAXIMIZED_BOTH);
		setResizable(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		panel = new JPanel();
		panel.setLayout(null);

		precisionTestButton = new JButton();
		precisionTestButton.setBackground(Color.red);
		resetButtonToMiddle();
		precisionTestButton.setVisible(false);
		precisionTestButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "none");

		reactionTestButton = new JButton();
		reactionTestButton.setBackground(Color.red);
		reactionTestButton.setVisible(false);
		reactionTestButton.getInputMap().put(KeyStroke.getKeyStroke("SPACE"), "none");

		precisionTestStartButton = new JButton("Precision");
		precisionTestStartButton.setBounds(SCREEN_WIDTH / 3, SCREEN_HEIGHT_MIDDLE, BUTTON_STARTING_WIDTH,
				BUTTON_STARTING_HEIGHT);
		precisionTestStartButton.addActionListener(precisionSetupButtonListener);

		reactionTestStartButton = new JButton("Reaction");
		reactionTestStartButton.setBounds(SCREEN_WIDTH * 2 / 3, SCREEN_HEIGHT_MIDDLE, BUTTON_STARTING_WIDTH,
				BUTTON_STARTING_HEIGHT);
		reactionTestStartButton.addActionListener(reactionSetupButtonListener);

		panel.add(precisionTestButton);
		panel.add(reactionTestButton);
		panel.add(precisionTestStartButton);
		panel.add(reactionTestStartButton);

		setFocusable(true);
		add(panel);
		addKeyListener(new HotKeyAdapter());
		setVisible(true);

	}

	public void start() {
		System.out.println("Initializing GUI");
		if (t == null) {
			t = new Thread(this, THREAD_NAME);
			t.start();
		}
	}

	private void startPrecisionTest() {
		precisionTestIteration++;
		currentField = 0;
		precisionTestButton.addActionListener(precisionStartButtonListener);
		resetButtonToMiddle();
		setButtonHeight(BUTTON_STARTING_HEIGHT);
		setButtonWidth(BUTTON_STARTING_WIDTH);
		if (precisionTestIteration % 2 == 0) {
			shrinkButton();
		}
		precisionTestButton.setBackground(Color.green);
		precisionTestButton.setVisible(true);
	}

	private void startReactionTest() {
		reactionTestIteration++;
		currentField = 0;
		reactionTestButton.addActionListener(reactionButtonListener);
		reactionTestButton.setVisible(false);
		reactionTestButton.setBounds(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		makeButtonVisibleAndStartTimer();
	}

	private class PrecisionSetupButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			hideSelectionButtons();
			startPrecisionTest();
		}
	}

	private class ReactionSetupButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			hideSelectionButtons();
			startReactionTest();
		}
	}

	private class PrecisionStartButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			startTime = System.currentTimeMillis();
			precisionTestButton.setBackground(Color.red);
			precisionTestButton.removeActionListener(precisionStartButtonListener);
			precisionTestButton.addActionListener(precisionButtonListener);
			precisionTestButton.setBounds(randomXPos(), randomYPos(), buttonWidth, buttonHeight);
			requestFocusInWindow();
		}
	}

	private class PrecisionButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			endTime = System.currentTimeMillis();
			timerArray[currentField] = calculateTimeElapsed();
			precisionTestButton.removeActionListener(precisionButtonListener);
			precisionTestButton.setBackground(Color.green);
			currentField++;
			if (currentField < TIMER_ARRAY_SIZE) {
				resetButtonToMiddle();
				precisionTestButton.addActionListener(precisionStartButtonListener);
				requestFocusInWindow();
			} else {
				resetButtonToMiddle();
				precisionTestButton.setVisible(false);
				System.out.println(precisionTestIteration + ". Precision test results: ");
				for (int i = 0; i < TIMER_ARRAY_SIZE; i++) {
					System.out.println("time " + (i + 1) + ": " + timerArray[i] + "s");
				}
				showSelectionButtons();
			}

		}
	}

	private class ReactionButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			endTime = System.currentTimeMillis();
			timerArray[currentField] = calculateTimeElapsed();
			currentField++;
			reactionTestButton.setVisible(false);
			if (currentField < TIMER_ARRAY_SIZE) {
				makeButtonVisibleAndStartTimer();
			} else {
				reactionTestButton.removeActionListener(reactionButtonListener);
				System.out.println(reactionTestIteration + ". Reaction test results: ");
				for (int i = 0; i < TIMER_ARRAY_SIZE; i++) {
					System.out.println("time " + (i + 1) + ": " + timerArray[i] + "s");
				}
				showSelectionButtons();
			}
		}
	}

	private void makeButtonVisibleAndStartTimer() {

		reactionTestButton.setBackground(Color.red);

		int delay = (int) Math.round(INITAL_DELAY + Math.random() * MAXIMUM_DELAY);
		Timer timer = new Timer(delay, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				reactionTestButton.setVisible(true);
				startTime = System.currentTimeMillis();
			}
		});
		timer.setRepeats(false);
		timer.start();

	}

	private int randomXPos() {
		int pos = (int) Math.round((Math.random() * SCREEN_WIDTH));
		if (isValidXPosition(pos)) {
			return pos;
		} else {
			return randomXPos();
		}
	}

	private int randomYPos() {
		int pos = (int) Math.round((Math.random() * SCREEN_HEIGHT));
		if (isValidYPosition(pos)) {
			return pos;
		} else {
			return randomYPos();
		}
	}

	private boolean isValidXPosition(int pos) {
		return (pos < SCREEN_WIDTH - buttonWidth && (pos < SCREEN_WIDTH * 0.35 || pos > SCREEN_WIDTH * 0.65));
	}

	private boolean isValidYPosition(int pos) {
		return (pos < SCREEN_HEIGHT - buttonHeight - TASKBAR_HEIGHT
				&& (pos < SCREEN_HEIGHT * 0.35 || pos > SCREEN_HEIGHT * 0.65));
	}

	private int getButtonStartPositionX() {
		return SCREEN_WIDTH_MIDDLE - buttonWidth / 2;
	}

	private int getButtonStartPositionY() {
		return SCREEN_HEIGHT_MIDDLE - buttonHeight / 2;
	}

	private void resetButtonToMiddle() {
		precisionTestButton.setBounds(getButtonStartPositionX(), getButtonStartPositionY(), BUTTON_STARTING_WIDTH,
				BUTTON_STARTING_HEIGHT);
	}

	private void shrinkButton() {
		setButtonWidth((int) Math.round(buttonWidth * BUTTON_SHRINK_PERCENTAGE));
		setButtonHeight((int) Math.round(buttonHeight * BUTTON_SHRINK_PERCENTAGE));
	}

	private void setButtonWidth(int width) {
		this.buttonWidth = width;
	}

	private void setButtonHeight(int height) {
		this.buttonHeight = height;
	}

	private double calculateTimeElapsed() {
		double timeDiffDouble = endTime - startTime;
		return new BigDecimal((timeDiffDouble) / 1000).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

	private void hideSelectionButtons() {
		precisionTestStartButton.setVisible(false);
		reactionTestStartButton.setVisible(false);
	}

	private void showSelectionButtons() {
		precisionTestStartButton.setVisible(true);
		reactionTestStartButton.setVisible(true);
	}

	private class HotKeyAdapter extends KeyAdapter {

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();

			switch (key) {
			case KeyEvent.VK_SPACE:
				MouseController.clickLeftMouseButton();
				break;

			case KeyEvent.VK_S:
				Main.switchEmote();
				if (currentEmote.equals("brow")) {
					currentEmote = "blink";
				} else {
					currentEmote = "brow";
				}
				JOptionPane.showMessageDialog(panel, "Used emote is now: " + currentEmote);
				break;

			case KeyEvent.VK_E:
				System.exit(200);
				break;

			case KeyEvent.VK_R:
				MouseController.resetCursor();
				Main.resetCursor();
				break;

			default:
				break;
			}
		}
	}

}
