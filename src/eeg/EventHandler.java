package eeg;

import com.emotiv.Iedk.Edk;
import com.emotiv.Iedk.EdkErrorCode;
import com.emotiv.Iedk.EmoState;
import com.sun.jna.Pointer;

/**
 * This class handles Events regarding EmoStates and Facial Expressions.
 * 
 * @author Chris
 *
 */
public class EventHandler implements Runnable {

	private Thread eventThread;
	private final String THREAD_NAME = "EventHandlerThread";

	private boolean BROW_USED = true;
	private final int RAISE_BROW_STATE = 32;
	private int state = 0;

	@Override
	public void run() {

		Thread thisThread = Thread.currentThread();

		Pointer eEvent = Edk.INSTANCE.IEE_EmoEngineEventCreate();
		Pointer eState = Edk.INSTANCE.IEE_EmoStateCreate();

		while (eventThread == thisThread) {
			state = Edk.INSTANCE.IEE_EngineGetNextEvent(eEvent);

			// New event needs to be handled
			if (state == EdkErrorCode.EDK_OK.ToInt()) {

				int eventType = Edk.INSTANCE.IEE_EmoEngineEventGetType(eEvent);

				// Log the EmoState if it has been updated
				if (eventType == Edk.IEE_Event_t.IEE_EmoStateUpdated.ToInt()) {

					Edk.INSTANCE.IEE_EmoEngineEventGetEmoState(eEvent, eState);

					if (BROW_USED) {

						// Raising brow
						if (EmoState.INSTANCE.IS_FacialExpressionGetUpperFaceAction(eState) == RAISE_BROW_STATE) {
							MouseController.clickLeftMouseButton();
						}
					} else {

						// Blinking
						if (EmoState.INSTANCE.IS_FacialExpressionIsBlink(eState) == 1) {
							MouseController.clickLeftMouseButton();
						}
					}

				}
			} else if (state != EdkErrorCode.EDK_NO_EVENT.ToInt()) {
				System.out.println("Internal error in Emotiv Engine!");
				break;
			}
		}

		Edk.INSTANCE.IEE_EngineDisconnect();
		System.out.println("Disconnected!");

	}

	/**
	 * Switches the currently used facial gesture from Raise brow to Blink and
	 * vice versa
	 */
	public void switchEmote() {
		BROW_USED = !BROW_USED;
	}

	public void start() {
		System.out.println("Initializing EventHandler");
		if (eventThread == null) {
			eventThread = new Thread(this, THREAD_NAME);
			eventThread.start();
		}
	}

	public void stop() {
		eventThread = null;
	}

}
