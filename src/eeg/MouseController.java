package eeg;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.InputEvent;

/**
 * This class is the endpoint for mouse movement and clicks.
 * 
 * @author Chris
 *
 */
public class MouseController {

	private final static Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	private final static int SCREEN_WIDTH = SCREEN_SIZE.width;
	private final static int SCREEN_HEIGHT = SCREEN_SIZE.height;
	private static Robot robot;

	public MouseController() {
		try {
			robot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}

	public static void moveMouseCursor(int x, int y) {
		robot.mouseMove(x, y);
	}

	public static void clickLeftMouseButton() {
		robot.mousePress(InputEvent.BUTTON1_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_MASK);
	}

	public static void clickRightMouseButton() {
		robot.mousePress(InputEvent.BUTTON3_MASK);
		robot.mouseRelease(InputEvent.BUTTON3_MASK);
	}

	/**
	 * Places the cursor to the middle of the screen
	 */
	public static void resetCursor() {
		robot.mouseMove(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	}
}
