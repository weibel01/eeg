package eeg;

import java.awt.AWTException;

import com.emotiv.Iedk.Edk;
import com.emotiv.Iedk.EdkErrorCode;

import view.SwingGUI;

/**
 * Main class for starting the handlers and GUI.
 * 
 * @author Chris
 *
 */
public class Main {

	private static SwingGUI swing;
	private static EventHandler eventHandler;
	private static GyroHandler gyroHandler;

	public static void main(String[] args) throws AWTException, InterruptedException {

		if (Edk.INSTANCE.IEE_EngineConnect("Emotiv Systems-5") != EdkErrorCode.EDK_OK.ToInt()) {
			System.out.println("Emotiv Engine start up failed.");
			return;
		}

		new MouseController();
		swing = new SwingGUI();
		eventHandler = new EventHandler();
		gyroHandler = new GyroHandler();
		swing.start();
		gyroHandler.start();
		eventHandler.start();
	}

	public static void stopGyroHandler() {
		gyroHandler.stop();
	}

	public static void stopEventHandler() {
		eventHandler.stop();
	}

	public static void resetCursor() {
		gyroHandler.resetInternalMousePos();
	}

	public static void switchEmote() {
		eventHandler.switchEmote();
	}
}
