package eeg;

import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Toolkit;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.emotiv.Iedk.Edk;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;

/**
 * This class handles the gyroscope and converts the data to mouseMovement
 * 
 * @author Chris
 *
 */
public class GyroHandler implements Runnable {

	private Thread gyroThread;
	private final String THREAD_NAME = "GyroHandlerThread";
	private final float BUFFERSIZE_IN_SEC = 0.1F;

	private final Dimension SCREEN_SIZE = Toolkit.getDefaultToolkit().getScreenSize();
	private final int SCREEN_WIDTH = SCREEN_SIZE.width;
	private final int SCREEN_HEIGHT = SCREEN_SIZE.height;

	private double xRestPosition = 8186.61;
	private double yRestPosition = 8179.09;
	private final int CALIBRATION_ARRAY_SIZE = 255;
	private double[] xValues = new double[CALIBRATION_ARRAY_SIZE];
	private int xCounter = 0;
	private double[] yValues = new double[CALIBRATION_ARRAY_SIZE];
	private int yCounter = 0;
	private boolean xAxisCalibrated = false;
	private boolean yAxisCalibrated = false;

	private final int NOISE_REDUCTION_VALUE = 10;

	private int internalXPos = SCREEN_WIDTH / 2; // initial X Position of
													// the cursor
	private int internalYPos = SCREEN_HEIGHT / 2; // initial Y Position of
													// the cursor
	private int sensitivityModifier = 15; // higher means less sensitive

	@Override
	public void run() {

		Thread thisThread = Thread.currentThread();

		IntByReference nSamplesTaken = null;
		nSamplesTaken = new IntByReference(0);

		Pointer hMotionData = Edk.INSTANCE.IEE_MotionDataCreate();

		Edk.INSTANCE.IEE_MotionDataSetBufferSizeInSec(BUFFERSIZE_IN_SEC);

		// Cursor is moved to the initial standard Position
		MouseController.moveMouseCursor(internalXPos, internalYPos);

		while (gyroThread == thisThread) {

			Edk.INSTANCE.IEE_MotionDataUpdateHandle(0, hMotionData);
			Edk.INSTANCE.IEE_MotionDataGetNumberOfSample(hMotionData, nSamplesTaken);

			if (nSamplesTaken != null && nSamplesTaken.getValue() != 0) {

				int yPos = internalYPos;
				int xPos = internalXPos;

				double[] data = new double[nSamplesTaken.getValue()];

				for (int sampleIdx = 0; sampleIdx < nSamplesTaken.getValue(); ++sampleIdx) {
					for (int i = 1; i < 3; i++) {

						// Fills the data array with recorded motion data
						Edk.INSTANCE.IEE_MotionDataGet(hMotionData, i, data, nSamplesTaken.getValue());

						double currentValue = data[sampleIdx];

						// horizontal acceleration
						if (i == 1) {

							if (!xAxisCalibrated) {

								xValues[xCounter] = currentValue;
								xCounter++;
								if (xCounter == CALIBRATION_ARRAY_SIZE) {
									xRestPosition = calculateAverage(xValues);
									xAxisCalibrated = true;
								}

							} else {

								xPos = getMouseXPos();

								// calculate the difference to the current
								// xPosition
								double posDiff = currentValue - xRestPosition;
								posDiff = normalizeDifference(posDiff);
								int actualDiff = (int) Math.round((posDiff / sensitivityModifier));

								// checks whether the internal X Position is
								// within screen bounds
								// if so, the new actual xPos is calculated
								if (internalXPos >= 0 && internalXPos <= SCREEN_WIDTH) {
									xPos = xPos - actualDiff;
								}

								// internal Position is always calculated
								internalXPos = internalXPos - actualDiff;

								yPos = getMouseYPos(); // yPos stays the same
														// while
														// xPos changes

							}
						}

						// vertical acceleration
						if (i == 2) {

							if (!yAxisCalibrated) {

								yValues[yCounter] = currentValue;
								yCounter++;
								if (yCounter == CALIBRATION_ARRAY_SIZE) {
									yRestPosition = calculateAverage(yValues);
									yAxisCalibrated = true;
								}

							} else {

								yPos = getMouseYPos();

								// calculate the difference to the current
								// yPosition
								double posDiff = currentValue - yRestPosition;
								posDiff = normalizeDifference(posDiff);
								int actualDiff = (int) Math.round((posDiff / sensitivityModifier));

								// checks whether the internal Y Position is
								// within screen bounds
								// if so, the new actual yPos is calculated
								if (internalYPos >= 0 && internalYPos <= SCREEN_HEIGHT) {
									yPos = yPos + actualDiff;
								}

								// internal Position is always calculated
								internalYPos = internalYPos + actualDiff;

								xPos = getMouseXPos(); // xPos stays the same
														// while
														// yPos changes
							}
						}

						if (xAxisCalibrated && yAxisCalibrated) {
							// The actual movement of the cursor
							MouseController.moveMouseCursor(xPos, yPos);
						}
					}
				}
			}
		} // end while
	}

	private int getMouseXPos() {
		return MouseInfo.getPointerInfo().getLocation().x;
	}

	private int getMouseYPos() {
		return MouseInfo.getPointerInfo().getLocation().y;
	}

	/**
	 * resets the internal mouse position to the middle of the screen
	 */
	public void resetInternalMousePos() {
		internalXPos = SCREEN_WIDTH / 2;
		internalYPos = SCREEN_HEIGHT / 2;
	}

	/**
	 * Checks a given difference to the previous position and sets it to zero if
	 * it is smaller than the {@link #NOISE_REDUCTION_VALUE}
	 */
	private double normalizeDifference(double diff) {
		if (Math.abs(diff) < NOISE_REDUCTION_VALUE) {
			return 0;
		}
		return diff;
	}

	private double calculateAverage(double[] values) {
		double sum = 0;
		for (int i = 0; i < values.length; i++) {
			sum += values[i];
		}
		return new BigDecimal(sum / values.length).setScale(2, RoundingMode.HALF_UP).doubleValue();
	}

	public void start() {
		System.out.println("Initializing GyroHandler");
		gyroThread = new Thread(this, THREAD_NAME);
		gyroThread.start();
	}

	public void stop() {
		gyroThread = null;
	}

}
